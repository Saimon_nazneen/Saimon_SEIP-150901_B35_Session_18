<?php
//create function in class and set their property and show them;
class BITM {
    public $window;                        //Property or global variable;
    public $door;
    public $chair;
    public $table;
    public $whiteboard;

    public function airCooler(){          //method
        echo "I'm cooling the Air";
    }

    public function compute(){
        echo "I'm computing";
    }
    public function show(){
        echo $this-> window."<br>";
        echo $this-> door."<br>";
        echo $this-> table."<br>";
        echo $this-> chair."<br>";
        echo $this-> whiteboard."<br>";
    }

    public function setWindow($win)   //For Property settings;
    {
        $this->window = $win;    // $this is a special variable;
                                // $this indicates global variable
    }

    public function setDoor($do)      //$do, $win are local variable;
    {
        $this->door = $do;
    }

    public function setChair($cha)
    {
        $this->chair = $cha;
    }

    public function setTable($tab)
    {
        $this->table = $tab;
    }

    public function setWhiteboard($board)
    {
        $this->whiteboard = $board;
    }
   /* public function setAll()
    {
        $this->setChair("I'm a Chair");                     //not required;
        $this->setTable("I'm a Table");

    }*/
}

// end of class;

$obj_BITM_at_Ctg = new BITM;           //create object;

$obj_BITM_at_Ctg ->setChair("I'm a Chair"); //value of Property;
$obj_BITM_at_Ctg ->setTable("I'm a Table");
$obj_BITM_at_Ctg ->setDoor("I'm a Door");
$obj_BITM_at_Ctg ->setwhiteboard("I'm a whiteboard");
$obj_BITM_at_Ctg ->setWindow("I'm a window");

//$obj_BITM_at_Ctg -> setAll();

$obj_BITM_at_Ctg ->show();


//Inheritance;

class lab402 extends BITM{
     public function setChair()
    {
        parent ::setchair("Nazneen Sultana Saimon");

    }
}
$objBITM_Lab = new Lab402();

$objBITM_Lab ->setChair("This is a Chair");
$objBITM_Lab ->setTable("This is a Table");
$objBITM_Lab ->setDoor("This is a Door");
$objBITM_Lab ->setwhiteboard("This is a whiteboard");
$objBITM_Lab ->setWindow("This is a window");
$objBITM_Lab -> show();