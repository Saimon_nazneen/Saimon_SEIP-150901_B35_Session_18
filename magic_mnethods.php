<?php
class ImAClass{
    public static $myMsg;
    public static function message(){
        echo self :: $myMsg;
    }
    public function __construct($value)
    {
        echo $value."<br>";
    }
    public function __destruct()
    {
        echo "Good bye"."<br>";
    }
    public function __call($name, $arguments)
    {
        echo $name."<br>";
        print_r($arguments);
    }
    public function __callStatic($name, $arguments)
    {

    }
}

ImAClass :: message();
$obj = new ImAClass("Hello World");
$obj -> show(35, 54, 3);