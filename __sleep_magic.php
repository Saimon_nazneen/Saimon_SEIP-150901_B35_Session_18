<?php
class AnyClass{
    public $myproperty1=8735;
    public $myProperty2="gifdkbvkd";
    public $otherProperty=44645;
    public function __sleep()
    {
       echo "I'm inside sleep magic method<br>";
        return array("myProperty1,myProperty2");
    }
    public function __wakeup()
    {
        echo "I'm inside wakeup magic method<br>";
    }
    public function __toString()
    {
        echo $this -> myproperty1."<br>";
        echo $this -> myProperty2."<br>";
        echo $this -> otherProperty."<br>";
        $this -> myMethod();
        return "You cannot  echo an object directly";
    }
    public function myMethod(){
        echo "hello";
    }
}
$obj = new AnyClass();
$serializeString = serialize($obj);
echo $serializeString."<br>";

$objAnother = unserialize($serializeString);
print_r ($objAnother);
